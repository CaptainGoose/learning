# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from app.models import MainSlider, MainHeader, Subject, Category, Theme, Data

admin.site.register(MainSlider)
admin.site.register(MainHeader)
admin.site.register(Subject)
admin.site.register(Category)
admin.site.register(Theme)
admin.site.register(Data)