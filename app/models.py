# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser, User
from django.db import models


# Create your models here.

class MainSlider(models.Model):
    image = models.ImageField(upload_to='media/img/', verbose_name='Main Slider')
    title = models.CharField(max_length=255, verbose_name='Slider Title')

    def __str__(self):
        return self.image.name


class MainHeader(models.Model):
    image = models.ImageField(upload_to='media/img/', verbose_name='Header icon')
    title = models.CharField(max_length=255, verbose_name='Title')
    body = models.TextField(verbose_name='Header Body')

    def __str__(self):
        return str(self.title)


class Subject(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название курса')

    def __str__(self):
        return str(self.name)


class Category(models.Model):
    subject = models.ForeignKey(Subject, related_name='category')
    name = models.CharField(max_length=255, verbose_name='Название категории')

    def __str__(self):
        return str(self.name)


class Theme(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название темы')
    category = models.ForeignKey(Category, related_name='theme')

    def __str__(self):
        return str(self.name)


class Data(models.Model):
    name = models.CharField(max_length=255, verbose_name='Данные')
    audio = models.FileField(verbose_name='Аудио файл', upload_to='media/audio/')

    def __str__(self):
        return str(self.name)


class UserProfile(models.Model):
    phone = models.CharField(max_length=255, verbose_name='Phone')
    user = models.OneToOneField(User, related_name='phone')

    def __str__(self):
        return str(self.phone + ' ' + self.user.username)
