# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, logout
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.base import View

from app.models import MainSlider, MainHeader, Subject, Category, Theme, UserProfile


# @api_view(['GET'])
# def api_root(request, format=None):
#     """
#     The entry endpoint of our API.
#     """
#     return Response({
#         'users': reverse('user-list', request=request),
#         'groups': reverse('group-list', request=request),
#     })
#
#
# class UserList(generics.ListCreateAPIView):
#     """
#     API endpoint that represents a list of users.
#     """
#     queryset = User.objects.all()
#     model = User
#     serializer_class = UserSerializer
#
#
# class UserDetail(generics.RetrieveUpdateDestroyAPIView):
#     """
#     API endpoint that represents a single user.
#     """
#     model = User
#     serializer_class = UserSerializer
#     def get_queryset(self):
#         query_set = User.objects.filter(pk=self.kwargs['pk'])
#         return query_set


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data()
        context['sliders'] = MainSlider.objects.all()
        context['headers'] = MainHeader.objects.all()
        return context


class CategoriesView(TemplateView):
    template_name = 'category.html'

    def get_context_data(self, **kwargs):
        context = super(CategoriesView, self).get_context_data()
        context['subject'] = Subject.objects.all()
        return context


class CategoryView(TemplateView):
    template_name = 'categories.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data()
        context['subject'] = Subject.objects.all()
        context['categories'] = Category.objects.filter(subject_id=kwargs['subject_id'])
        return context


class ThemesView(TemplateView):
    template_name = 'theme.html'

    def get_context_data(self, **kwargs):
        context = super(ThemesView, self).get_context_data()
        context['subjects'] = Subject.objects.all()
        context['category'] = Category.objects.get(id=kwargs['category_id'])
        context['themes'] = Theme.objects.filter(category_id=kwargs['category_id'])
        return context


# class Login(View, LoginView):

def registration(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        phone = request.POST.get('phone')
        password = request.POST.get('password')
        if username and phone and password:
            User.objects.create(
                username=username,
                password=password
            )
            UserProfile.objects.create(
                user=username,
                phone=phone
            )
            return redirect(reverse('categories'))
    return redirect(reverse('index'))


def auth_logout(request):
    logout(request)
    return redirect(reverse("index"))


def redirect_home(request):
    return redirect(reverse("index"))


class ProfileView(TemplateView):
    template_name = 'profile.html'


class PreLessonView(TemplateView):
    template_name = 'lesson.html'
